<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>4.8.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>json</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">PngQuantLow</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png8</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../sprite.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">belt_01.png</key>
            <key type="filename">belt_02.png</key>
            <key type="filename">belt_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>250,38,500,75</rect>
                <key>scale9Paddings</key>
                <rect>250,38,500,75</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">blood_001.png</key>
            <key type="filename">blood_002.png</key>
            <key type="filename">blood_003.png</key>
            <key type="filename">blood_004.png</key>
            <key type="filename">blood_005.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>54,27,107,53</rect>
                <key>scale9Paddings</key>
                <rect>54,27,107,53</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">cut1_01.png</key>
            <key type="filename">cut1_02.png</key>
            <key type="filename">cut1_03.png</key>
            <key type="filename">cut2_01.png</key>
            <key type="filename">cut2_02.png</key>
            <key type="filename">cut2_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>101,54,203,107</rect>
                <key>scale9Paddings</key>
                <rect>101,54,203,107</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">hair_01_1.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,22,48,43</rect>
                <key>scale9Paddings</key>
                <rect>24,22,48,43</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">hair_02.png</key>
            <key type="filename">hair_03.png</key>
            <key type="filename">hair_04.png</key>
            <key type="filename">hair_05.png</key>
            <key type="filename">hair_06.png</key>
            <key type="filename">hair_07.png</key>
            <key type="filename">hair_08.png</key>
            <key type="filename">hair_09.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>42,56,85,113</rect>
                <key>scale9Paddings</key>
                <rect>42,56,85,113</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">scan_001.png</key>
            <key type="filename">scan_002.png</key>
            <key type="filename">scan_003.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>108,11,215,21</rect>
                <key>scale9Paddings</key>
                <rect>108,11,215,21</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">scroll_meowmie1_01.png</key>
            <key type="filename">scroll_meowmie1_02.png</key>
            <key type="filename">scroll_meowmie1_03.png</key>
            <key type="filename">scroll_meowmie1_04.png</key>
            <key type="filename">scroll_meowmie1_05.png</key>
            <key type="filename">scroll_meowmie1_06.png</key>
            <key type="filename">scroll_meowmie1_07.png</key>
            <key type="filename">scroll_meowmie2_00.png</key>
            <key type="filename">scroll_meowmie2_01.png</key>
            <key type="filename">scroll_meowmie2_02.png</key>
            <key type="filename">scroll_meowmie2_03.png</key>
            <key type="filename">scroll_meowmie2_04.png</key>
            <key type="filename">scroll_meowmie2_05.png</key>
            <key type="filename">scroll_meowmie2_06.png</key>
            <key type="filename">scroll_meowmie2_07.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>56,56,111,111</rect>
                <key>scale9Paddings</key>
                <rect>56,56,111,111</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">sign_03.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>126,85,252,171</rect>
                <key>scale9Paddings</key>
                <rect>126,85,252,171</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">space_01.png</key>
            <key type="filename">space_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,14,49,29</rect>
                <key>scale9Paddings</key>
                <rect>24,14,49,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">tap_01.png</key>
            <key type="filename">tap_02.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>14,17,27,33</rect>
                <key>scale9Paddings</key>
                <rect>14,17,27,33</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">victim1_01.png</key>
            <key type="filename">victim1_happy_01.png</key>
            <key type="filename">victim1_happy_02.png</key>
            <key type="filename">victim1_shock_01.png</key>
            <key type="filename">victim2_01.png</key>
            <key type="filename">victim2_happy_01.png</key>
            <key type="filename">victim2_happy_02.png</key>
            <key type="filename">victim2_shock_01.png</key>
            <key type="filename">victim3_01.png</key>
            <key type="filename">victim3_happy_01.png</key>
            <key type="filename">victim3_happy_02.png</key>
            <key type="filename">victim3_shock_01.png</key>
            <key type="filename">victim4_01.png</key>
            <key type="filename">victim4_happy_01.png</key>
            <key type="filename">victim4_happy_02.png</key>
            <key type="filename">victim4_shock_01.png</key>
            <key type="filename">victim5_01.png</key>
            <key type="filename">victim5_happy_01.png</key>
            <key type="filename">victim5_happy_02.png</key>
            <key type="filename">victim5_shock_01.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>82,128,164,256</rect>
                <key>scale9Paddings</key>
                <rect>82,128,164,256</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>scan_001.png</filename>
            <filename>scan_002.png</filename>
            <filename>scan_003.png</filename>
            <filename>scroll_meowmie1_01.png</filename>
            <filename>scroll_meowmie1_02.png</filename>
            <filename>scroll_meowmie1_03.png</filename>
            <filename>scroll_meowmie1_04.png</filename>
            <filename>scroll_meowmie1_05.png</filename>
            <filename>scroll_meowmie1_06.png</filename>
            <filename>scroll_meowmie1_07.png</filename>
            <filename>scroll_meowmie2_00.png</filename>
            <filename>scroll_meowmie2_01.png</filename>
            <filename>scroll_meowmie2_02.png</filename>
            <filename>scroll_meowmie2_03.png</filename>
            <filename>scroll_meowmie2_04.png</filename>
            <filename>scroll_meowmie2_05.png</filename>
            <filename>scroll_meowmie2_06.png</filename>
            <filename>scroll_meowmie2_07.png</filename>
            <filename>sign_03.png</filename>
            <filename>space_01.png</filename>
            <filename>space_02.png</filename>
            <filename>tap_01.png</filename>
            <filename>tap_02.png</filename>
            <filename>victim1_01.png</filename>
            <filename>victim1_happy_01.png</filename>
            <filename>victim1_happy_02.png</filename>
            <filename>victim1_shock_01.png</filename>
            <filename>victim2_01.png</filename>
            <filename>victim2_happy_01.png</filename>
            <filename>victim2_happy_02.png</filename>
            <filename>victim2_shock_01.png</filename>
            <filename>victim3_01.png</filename>
            <filename>victim3_happy_01.png</filename>
            <filename>victim3_happy_02.png</filename>
            <filename>victim3_shock_01.png</filename>
            <filename>victim4_01.png</filename>
            <filename>victim4_happy_01.png</filename>
            <filename>victim4_happy_02.png</filename>
            <filename>victim4_shock_01.png</filename>
            <filename>victim5_01.png</filename>
            <filename>victim5_happy_01.png</filename>
            <filename>victim5_happy_02.png</filename>
            <filename>victim5_shock_01.png</filename>
            <filename>belt_01.png</filename>
            <filename>belt_02.png</filename>
            <filename>belt_03.png</filename>
            <filename>blood_001.png</filename>
            <filename>blood_002.png</filename>
            <filename>blood_003.png</filename>
            <filename>blood_004.png</filename>
            <filename>blood_005.png</filename>
            <filename>cut1_01.png</filename>
            <filename>cut1_02.png</filename>
            <filename>cut1_03.png</filename>
            <filename>cut2_01.png</filename>
            <filename>cut2_02.png</filename>
            <filename>cut2_03.png</filename>
            <filename>hair_01_1.png</filename>
            <filename>hair_02.png</filename>
            <filename>hair_03.png</filename>
            <filename>hair_04.png</filename>
            <filename>hair_05.png</filename>
            <filename>hair_06.png</filename>
            <filename>hair_07.png</filename>
            <filename>hair_08.png</filename>
            <filename>hair_09.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
