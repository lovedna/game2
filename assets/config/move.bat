rem echo d |xcopy image bin\image /S /E /Y
md output\font
md output\image
md output\sprite
md output\audio
md output\audio

echo d |xcopy image output\image /S /E /Y
echo d |xcopy audio output\audio /S /E /Y
copy sprite\sprite.png output\sprite\sprite.png
copy sprite\sprite.png.meta.json output\sprite\sprite.png.meta.json

rem echo d |xcopy assets/ui\assets\sim\image output\image /S /E /Y
rem copy assets/font\char_0.png output\font\char_0.png
rem copy assets/font\char.png output\font\char.png
rem copy assets/font\char_0.png.meta.json output\font\char_0.png.meta.json

copy table\TableClass.hx ..\src\table\TableClass.hx
copy  table\table.bytes output\table.bytes
rem copy assets/ui\output\sim.data output\sim.data

