rem echo OFF
set to=D:\www\game2
set from=assets\html5
if not "%1"=="min" goto end
if "%1"=="min" goto MINGAME
goto end


:MINGAME
if exist assets\closure-compiler-v20180402.jar (
java -jar assets\closure-compiler-v20180402.jar --compilation_level=SIMPLE_OPTIMIZATIONS --js %from%\game.js --js_output_file game.min.js
copy game.min.js  %from%\game.js
del /s/f game.min.js
)
goto end

:end

if exist %from%\game.js (
	echo d |xcopy %from%\lib %to%\lib /S /E /Y
	copy %from%\game.js %to%\game.js
	copy %from%\index.html %to%\index.html
	copy %from%\version.js %to%\version.js
)
set output=assets\output
if exist %output% (
	if exist %to%\assets (
	rem echo yes | del /s/f/q %to%\assets
		rmdir /s/q %to%\assets
	)
	echo d |xcopy assets\output %to%\assets /S /E /Y
	rmdir /s/q %output% 
)

rem exit