package;
import lovedna.game.components.ActorFrame;
import lovedna.game.components.Component;
import lovedna.game.events.ActorEvent;


/**
 * ... 
 * @author liwei
 */
class FrameAni extends Component
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	public var playing:Bool;
	public var frameTime:Float;
	/*======Private==============================*/
	private var _frame:ActorFrame;
	private var _frameTime:Float;
	public function new() 
	{
		super();
		
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	override function onAwake():Void 
	{
		super.onAwake();
		playing = true;
		frameTime = 0.1;
	}
	override function onStart():Void 
	{
		super.onStart();
		_frame = actor.getComponent(ActorFrame);
		bindEvent(ActorEvent.onUpdate, onUpdate);
		_frameTime = frameTime;
	}
	/*======Private methods======================*/
	private function onTriggerFrame():Void
	{
		_frame.nextFrame();
	}
	private function onUpdate(?e):Void
	{
		if (!playing) return;
		var dt:Float = actor.deltaTime;
		_frameTime-= dt;
		if (_frameTime <= 0)
		{
			_frameTime+= frameTime;
			onTriggerFrame();
		}
	}
	/*======get set methods======================*/
	
}