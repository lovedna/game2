package;
import lovedna.game.components.Component;
import lovedna.game.events.ActorEvent;
import lovedna.math.Vector3;


/**
 * ... 
 * @author liwei
 */
class MoveShow extends Component
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	
	/*======Private==============================*/
	private var _pos:Vector3;
	private var _callback:Void->Void;
	public function new() 
	{
		super();
		
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	public function play(callback:Void->Void):Void
	{
		_callback = callback;
		bindEvent(ActorEvent.onUpdate, onUpdate);
	}
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	override function onStart():Void 
	{
		super.onStart();
		_pos = actor.transform.position;
	}
	/*======Private methods======================*/
	private function onUpdate(?e):Void
	{
		var dt:Float = actor.deltaTime;
		_pos.x -= dt * 200;
		if (_pos.x <= 0)
		{
			_pos.x = 0;
			bindEvent(ActorEvent.onUpdate, null);
			if (_callback != null)
			{
				_callback();
				_callback = null;
			}
		}
	}
	/*======get set methods======================*/
	
}