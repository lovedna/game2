package table;
import haxe.ds.IntMap;
import lovedna.utils.Helper;
import table.TableClass.TableItemData;
import table.TableClass.TableLoaderItemBase;


/**
 * ... 
 * @author liwei
 */
class TableLoaderItem extends TableLoaderItemBase
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	
	/*======Private==============================*/
	private var _map:IntMap<TableItemData>;
	private var _t:Float =-1;
	public function new() 
	{
		super();
		_map = new IntMap<TableItemData>();
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	public function get(id:Int):TableItemData
	{
		return _map.get(id);
	}
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	override function addItem(item:TableItemData):Void 
	{
		super.addItem(item);
		_map.set(item.id, item);
		var list:Array<Float> = item.value;
		var len:Int = list.length;
		//trace(len);
		for(i in 0...len)
		{
			var v:Float = list[i];
			var offset:Float = v - item.start;
			var k:Int = Helper.floatToInt(offset / item.bar);
			var nv:Float=item.start + k * item.bar;
			list[i] = nv;
			//trace(i,v,Helper.formatTime(nv));
		}
	}
	/*======Private methods======================*/
	
	/*======get set methods======================*/
	
}