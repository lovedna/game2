package table;
import haxe.ds.StringMap;
import table.TableClass.TableLoaderBase;

/**
 * ... 
 * @author liwei
 */
class Tables
{
	/*======constants============================*/
	
	/*======static ==============================*/
	public static var item(default, null):TableLoaderItem;
	/*======Public===============================*/
	
	/*======Private==============================*/
	private static var _map:StringMap<TableLoaderBase>;
	private static var _select:TableLoaderBase;
	public function new() 
	{
		
	}
	/*======Public Static methods================*/
	@:noCompletion
	private static function init():Void
	{
		if (_map != null) return;
		_map = new StringMap<TableLoaderBase>();
		item = add(new TableLoaderItem());
	}
	@:noCompletion
	public static function selectTable(name:String):Void
	{
		init();
		_select = _map.get(name);
		if (_select == null) return;
		_select.onBeforeLoad();
	}
	@:noCompletion
	public static function addItem(obj:Dynamic):Void
	{
		if (_select == null) return;
		_select.load(obj);
	}
	@:noCompletion
	public static function completeTable():Void
	{
		if (_select != null)
		{
			_select.onAfterLoad();
		}
	}
	private static function add<T:TableLoaderBase>(loader:T):T
	{
		_map.set(loader.name, loader);
		return cast loader;
	}
	/*======Public methods=======================*/
	
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	
	/*======get set methods======================*/
	
}