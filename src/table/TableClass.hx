package table;

class TableLoaderBase
{
	public var name:String;
	public function new()
	{
		
	}
	public function onBeforeLoad():Void
	{
		
	}
	public function load(obj:Dynamic):Void
	{
		
	}
	public function onAfterLoad():Void
	{
		
	}
}

@:keep
class TableItemData
{
	public var id:Int;
	public var start:Float;
	public var bar:Float;
	public var value:Array<Float>;

	public function new() {}
}

class TableLoaderItemBase extends TableLoaderBase
{
	public function new() {
		super();
		this.name = "item";
	}
	
	override public function load(obj:Dynamic):Void 
	{
		var item:TableItemData = new TableItemData();
		lovedna.utils.DynamicUtil.copyFieldTo(obj, item);
		addItem(item);
	}
	private function addItem(item:TableItemData):Void{}
}
