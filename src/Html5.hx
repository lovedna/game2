package;
import lovedna.game.Game;

/**
 * ... 
 * @author liwei
 */
class Html5
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	
	/*======Private==============================*/
	
	public function new() 
	{
		
	}
	/*======Public Static methods================*/
	public static function main():Void
	{
		lovedna.utils.macro.CompileTime.buildNumberToFile("assets/html5/version.js", "version=0x{0};");
		Game.main = new Client();
	}
	/*======Public methods=======================*/
	
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	
	/*======get set methods======================*/
	
}