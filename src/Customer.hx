package;
import lovedna.game.Actor;
import lovedna.game.components.ActorFrame;
import lovedna.game.components.Image;
import lovedna.math.Random;


/**
 * ... 
 * @author liwei
 */
class Customer extends Actor
{
	/*======constants============================*/
	
	/*======static ==============================*/
	private static var _r:Random = new Random();
	/*======Public===============================*/
	
	/*======Private==============================*/
	private var _imageList:Array<Image>;
	private var _nameList:Array<String>;
	private var _moveshow:MoveShow;
	private var _dance:Dance;
	public function new() 
	{
		super();
		_nameList = ["victim1","victim2","victim3","victim4","victim5"];
		_imageList = [];
		var ani:FrameAni = this.addComponent(FrameAni);
		ani.frameTime = 0.3;
		ani.playing = false;
		var aframe:ActorFrame = this.addComponent(ActorFrame);
		for (i in 0...4)
		{
			var child:Actor = new Actor();
			aframe.add(child);
			child.transform.position.y = 512 * 0.5;
			var img:Image = child.addComponent(Image);
			img.addMeshRenderer();
			_imageList.push(img);
		}
		this._moveshow = this.addComponent(MoveShow);
		this._dance = this.addComponent(Dance);
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	public function load():Void
	{
		var name:String = _nameList[_r.randomInt(0, _nameList.length)];
		var list:Array<String> = ["_01", "_happy_01", "_happy_02", "_shock_01"];
		for (i in 0...4)
		{
			var img:Image = _imageList[i];
			//img.transform.pivot.y = 0;
			img.load("sprite/sprite/"+name+list[i] + ".png");
		}
		this.transform.position.set(600, -512 * 0.5, 0);
	}
	public function show(callback:Void->Void):Void
	{
		_moveshow.play(callback);
	}
	public function dance(v:Bool=true):Void
	{
		if (v)
		{
			_dance.play(0.2);
		}else
		{
			_dance.stop();
		}
		
	}
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	
	/*======get set methods======================*/
	
}