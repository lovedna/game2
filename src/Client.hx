package ;
import lovedna.ds.ByteArray;
import lovedna.ds.StringTable;
import lovedna.events.Event;
import lovedna.events.MediaEvent;
import lovedna.game.Actor;
import lovedna.game.CameraClearFlags;
import lovedna.game.Game;
import lovedna.game.IGameMain;
import lovedna.game.World;
import lovedna.game.components.ActorFrame;
import lovedna.game.components.Camera;
import lovedna.game.components.Image;
import lovedna.game.components.UIRoot;
import lovedna.game.debug.Debug;
import lovedna.game.helper.DebugGame;
import lovedna.math.Random;
import lovedna.math.Vector3;
import lovedna.media.AudioClip;
import lovedna.resource.Assets;
import lovedna.resource.data.ResBytes;
import lovedna.task.DelayCall;
import lovedna.ui.TouchManager;
import lovedna.utils.Helper;
import table.Tables;

/**
 * ... 
 * @author liwei
 */
//@:expose("Client")
class Client implements IGameMain
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	
	/*======Private==============================*/
	private var game:Game;
	private var mapWorld:World;
	private var uiroot:UIRoot;
	public function new() 
	{
		
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	public function startGameMain():Void
	{
		Assets.init("assets");
		#if wx
		//Assets.init("https://lovedna.gitee.io/h5/jump/assets");
		#else
		
		#end
		trace("-------------------");
		game = new Game();
		game.userBackBuffer = false;
		var debug = new DebugGame(game);
		//debug.debugFrame();
		game.color.set(1, 0, 0, 0);
		game.onInit = onGameInit;
		
	}
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	private function onGameInit():Void
	{
		mapWorld = new World();
		mapWorld.name = "world";
		game.addWorld(mapWorld);
		
		
		var cameraParent = new Actor();
		cameraParent.name = "cameraparent";
		mapWorld.addChild(cameraParent);
		
		var actor = new Actor();
		actor.setParent(cameraParent);
		actor.name = "camera";
		var camera = actor.addComponent(Camera);
		//actor.addComponent(CameraControl);
		camera.clearFlags.type = CameraClearFlags.All;
		actor.transform.position.set(0, 0, 200).scale(1);
		actor.transform.rotation.lookAt(actor.transform.position, new Vector3());
		
		actor = new Actor();
		uiroot = actor.addComponent(UIRoot);
		uiroot.touchInput.enabled = true;
		uiroot.setCamera(camera);
		uiroot.width = 1000;
		uiroot.height = 500;
		mapWorld.addChild(actor);
		
		
		
		var version:String = Game.buildVersion + "";
		Assets.setVersion("version.txt", Helper.intToHex(new Random().get()));
		var res:ResBytes = Assets.bytes.get("version.txt");
		res.addEventListener(Event.complete.type, function(?e) {
			var res:ResBytes = Assets.bytes.get("version.txt");
			if (res.enabled)
			{
				var ba:ByteArray = Assets.bytes.get("version.txt").bytes;
				version = ba.toString();
			}
			Assets.version = version;
			Assets.loadMeta("version.data", metaSuccess);
		} );
		
		
	}
	private function metaSuccess():Void
	{
		if (TouchManager.focus)
		{
			onFocus();
			return;
		}
		TouchManager.onBegin.addEventListener(Event.focus.type, onFocus);
		
	}
	private function onFocus(?e):Void
	{
		TouchManager.onBegin.removeEventListener(Event.focus.type, onFocus);
		Assets.bytes.get("table.bytes").addEventListener(Event.complete.type,function(?e){
			var res:ResBytes = Assets.bytes.get("table.bytes");
			if (res.enabled)
			{
				var table:StringTable = new StringTable();
				table.onStart = function(){
					Tables.selectTable(table.name);
				};
				table.onItem = function(i){
					Tables.addItem(i);
				};
				var ba:ByteArray = res.bytes;
				var len:Int = ba.readUnsignedInt();
				while (len-- > 0)
				{
					table.decode(ba);
					Tables.completeTable();
				}
				init();
			}
		});
	}
	private function init():Void
	{
		//trace("loading");
		
		
		var stage:Actor = new Actor();
		stage.setParent(uiroot.actor);
		
		
		var train:Train = new Train();
		
		//train.setParent(stage);
		//return;
		var play:PlayStage = new PlayStage();
		play.setParent(stage);
	}
	/*======get set methods======================*/
	
}