package;
import lovedna.game.components.Component;
import lovedna.game.events.ActorEvent;
import lovedna.math.Vector3;
import lovedna.utils.Helper;


/**
 * ... 
 * @author liwei
 */
class Dance extends Component
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	
	/*======Private==============================*/
	private var _delay:Float;
	private var _time:Float;
	private var _scale:Vector3;
	public function new() 
	{
		super();
		
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	public function play(delay:Float=0.3):Void
	{
		_delay = delay;
		_time = 0;
		bindEvent(ActorEvent.onUpdate, onUpdate);
		_scale = actor.transform.scale;
	}
	public function stop():Void
	{
		bindEvent(ActorEvent.onUpdate, null);
	}
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	private function onUpdate(?e):Void
	{
		var dt:Float = actor.deltaTime;
		_time+= dt;
		if (_time > _delay)
		{
			_time-= _delay;
			_scale.y = 1;
			stop();
			return;
		}
		var s:Float = Math.sin(_time / _delay * Helper.PI2);
		_scale.y = 1+s*0.02;
	}
	/*======get set methods======================*/
	
}