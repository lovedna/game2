package;
import lovedna.game.Actor;
import lovedna.game.components.ActorFrame;
import lovedna.game.components.Image;


/**
 * ... 
 * @author liwei
 */
class Belt extends Actor
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	
	/*======Private==============================*/
	private var _ani:FrameAni;
	public function new() 
	{
		super();
		_ani = this.addComponent(FrameAni);
		_ani.playing = false;
		_ani.frameTime = 0.06;
		var aframe:ActorFrame = this.addComponent(ActorFrame);
		for (i in 1...4)
		{
			var child:Actor = new Actor();
			aframe.add(child);
			var img:Image = child.addComponent(Image);
			img.transform.width = 1100;
			img.addMeshRenderer();
			img.load("sprite/sprite/belt_0"+i+".png");
		}
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	public function play():Void
	{
		_ani.playing = true;
	}
	public function stop():Void
	{
		_ani.playing = false;
	}
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	
	/*======get set methods======================*/
	
}