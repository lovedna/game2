package;
import lovedna.game.Actor;
import lovedna.game.components.Image;
import lovedna.utils.DynamicPool;


/**
 * ... 
 * @author liwei
 */
class PlayStage extends Actor
{
	/*======constants============================*/
	
	/*======static ==============================*/
	private var _manpool:DynamicPool<Customer> = new DynamicPool<Customer>(function(){return new Customer(); });
	/*======Public===============================*/
	
	/*======Private==============================*/
	private var belt:Belt;
	private var current:Customer;
	private var _sound:Sound;
	public function new() 
	{
		super();
		var actor:Actor = new Actor();
		var img:Image = actor.addComponent(Image);
		img.addMeshRenderer();
		img.load("image/background_01.png");
		actor.setParent(this) ;
		
		
		
		belt = new Belt();
		belt.transform.position.y =-300 + 300 * 0.25;
		belt.setParent(this);
		
		
		current = _manpool.get();
		current.setParent(this);
		current.load();
		current.show(manshow);
		belt.play();
		_sound = this.addComponent(Sound);
		_sound.play(1);
		_sound.onBeat = onBeat;
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	private function manshow():Void
	{
		belt.stop();
	}
	private function onBeat():Void
	{
		current.dance();
	}
	/*======get set methods======================*/
	
}