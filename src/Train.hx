package;
import lovedna.game.Actor;
import lovedna.game.components.ActorFrame;
import lovedna.game.components.Image;


/**
 * ... 
 * @author liwei
 */
class Train extends Actor
{
	/*======constants============================*/
	
	/*======static ==============================*/
	
	/*======Public===============================*/
	
	/*======Private==============================*/
	
	public function new() 
	{
		super();
		var ani:FrameAni = this.addComponent(FrameAni);
		//ani.playing = false;
		ani.frameTime = 0.5;
		var list:Array<Int> = [1,2,3,4,5,6,7];
		var aframe:ActorFrame = this.addComponent(ActorFrame);
		for (i in 1...list.length)
		{
			var child:Actor = new Actor();
			aframe.add(child);
			var img:Image = child.addComponent(Image);
			img.addMeshRenderer();
			img.load("sprite/sprite/scroll_meowmie1_0"+list[i]+".png");
		}
		for (i in 1...7)
		{
			var child:Actor = new Actor();
			aframe.add(child);
			var img:Image = child.addComponent(Image);
			img.addMeshRenderer();
			img.load("sprite/sprite/scroll_meowmie2_0"+i+".png");
		}
	}
	/*======Public Static methods================*/
	
	/*======Public methods=======================*/
	
	/*======public override methods==============*/
	
	/*======private override methods=============*/
	
	/*======Private methods======================*/
	
	/*======get set methods======================*/
	
}